INSERT INTO `users` (`email`, `password`, `datetime_created`) VALUES
('johnsmith@gmail.com', 'passwordA', '2020-01-02 01:00:00'),
('juandelacruz@gmail.com', 'passwordB', '2020-01-02 02:00:00'),
('janesmith@gmail.com', 'passwordC', '2020-01-02 03:00:00'),
('mariadelacruz@gmail.com', 'passwordD', '2020-01-02 04:00:00'),
('johndoe@gmail.com', 'passwordE', '2020-01-02 05:00:00');

INSERT INTO `posts` (`user_id`, `title`, `content`, `datetime_posted`) VALUES
(1, 'First Code', 'Hello World!', '2020-01-02 01:00:00'),
(1, 'Second Code', 'Hello Earth!', '2020-01-02 02:00:00'),
(2, 'Third Code', 'Welcome to Mars!', '2020-01-02 03:00:00'),
(4, 'Fourth Code', 'Bye bye solar system!', '2020-01-02 04:00:00');

SELECT * FROM posts WHERE user_id = 1;

SELECT email, datetime_created FROM users;

UPDATE `posts` SET `content` = 'Hello to the people of the Earth!' WHERE `post_id` = 2;

DELETE FROM `users` WHERE `email` = 'johndoe@gmail.com';
